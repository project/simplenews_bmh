<?php

// admin UI
function simplenews_bmh_admin_settings() {
  // recalculate on return, if scoring values changed
  $recalculate = variable_get('simplenews_bmh_recalculate', FALSE);
  if ($recalculate) {
    simplenews_bmh_calculate_scores();
    drupal_set_message('Bounce scores have been recalculated.');
    variable_set('simplenews_bmh_recalculate', FALSE);
  }
  
  // make sure the library is present
  $path = libraries_get_path('PHPMailer-BMH');
  if (!is_dir($path)) drupal_set_message('The PHPmailer-BMH library must be installed before bounce handling will work.', 'error');
  
  $form = array();
  
  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => 'General settings',
    '#tree' => FALSE,
  );
  // frequency of checks
  $form['general']['simplenews_bmh_cron_interval'] = array(
    '#type' => 'select',
    '#title' => 'Polling interval',
    '#description' => 'How often should we check the mailbox for bounce reports? (Note that cron must run at least this often for this setting to properly take effect.)',
    '#default_value' => variable_get('simplenews_bmh_cron_interval', 86400),
    '#options' => array(
      '0'     => 'Never',
      '300'   => 'Every 5 minutes',
      '900'   => 'Every 15 minutes',
      '1800'  => 'Every 30 minutes',
      '3600'  => 'Every hour',
      '10800' => 'Every 3 hours',
      '21600' => 'Every 6 hours',
      '86400' => 'Every day',
    ),
  );
  
  $form['general']['simplenews_bmh_process_maxmessages'] = array(
    '#type' => 'textfield',
    '#size' => 5,
    '#title' => 'Message processing limit',
    '#default_value' => variable_get('simplenews_bmh_process_maxmessages', 0),
    '#description' => 'How many messages should be processed on each run? Set to 0 for unlimited.',
  );
  
  // automatic removal, or manual review?
  $link = l('bounce report', 'admin/content/simplenews/bounce_report');
  $form['general']['simplenews_bmh_process_automatically'] = array(
    '#type' => 'checkbox',
    '#title' => 'Remove bouncing emails automatically',
    '#description' => "Check this box if you want bounce scoring and address removal to happen automatically. Leave it unchecked if you want to handle this manually from the $link.",
    '#default_value' => variable_get('simplenews_bmh_process_automatically', 0),
  );
  
  // delete processed messages?
  $form['general']['simplenews_bmh_process_delete'] = array(
    '#type' => 'checkbox',
    '#title' => 'Delete bounced messages',
    '#description' => 'Uncheck this to move the messages to a "Processed" folder in your mail account. Check to delete them instead.',
    '#default_value' => variable_get('simplenews_bmh_process_delete', 1),
  );
  
  // manually trigger mailbox processing
  $form['general']['simplenews_bmh_process_link'] = array(
    '#type' => 'markup',
    '#value' => '<p>Use this link to check the bounce mailbox manually: ' . l('Process mailbox', 'simplenews_bmh/bounce_manual/1') . '<br />(This will process the bounced emails, but will not apply scoring or remove addresses from newsletters. It will also display a plain-text results page.)</p>',
  );
  
  
  // local mailbox? 
  $form['mailbox'] = array(
    '#type' => 'fieldset',
    '#title' => 'Local mailbox',
    '#tree' => FALSE,
    '#description' => 'If your bounced mail is in a local mailbox file, define that here.',

  );
  $form['mailbox']['simplenews_bmh_inbox_local_path'] = array(
    '#type' => 'textfield',
    '#title' => 'Mailbox location',
    '#default_value' => variable_get('simplenews_bmh_inbox_local_path', FALSE),
    '#description' => 'Note: if a local mailbox is defined here, the remote settings below are ignored.',
  );

  /*
   * removed -- no way to handle local maildirs that I can see
   * 
  $form['mailbox']['simplenews_bmh_inbox_local_type'] = array(
    '#type' => 'radios',
    '#title' => 'Mailbox type',
    '#options' => array( 'mbox' => 'mbox', 'maildir' => 'maildir' ),
    '#default_value' => variable_get('simplenews_bmh_inbox_local_type', FALSE),
    '#description' => '',
  );
  */
  
  
  // remote server?
  $form['remote'] = array(
    '#type' => 'fieldset',
    '#title' => 'Remote mailbox',
    '#tree' => FALSE,
    '#description' => 'Provide the login settings for a remote mailbox.',
  );
  $form['remote']['simplenews_bmh_inbox_remote_host'] = array(
    '#type' => 'textfield',
    '#title' => 'Hostname',
    '#default_value' => variable_get('simplenews_bmh_inbox_remote_host', FALSE),
  );
  $form['remote']['simplenews_bmh_inbox_remote_user'] = array(
    '#type' => 'textfield',
    '#title' => 'Username',
    '#default_value' => variable_get('simplenews_bmh_inbox_remote_user', FALSE),
  );
  $form['remote']['simplenews_bmh_inbox_remote_pass'] = array(
    '#type' => 'textfield',
    '#title' => 'Password',
    '#default_value' => variable_get('simplenews_bmh_inbox_remote_pass', FALSE),
  );
  $form['remote']['simplenews_bmh_inbox_remote_port'] = array(
    '#type' => 'textfield',
    '#title' => 'Port',
    '#default_value' => variable_get('simplenews_bmh_inbox_remote_port', 143),
  );
  $form['remote']['simplenews_bmh_inbox_remote_service'] = array(
    '#type' => 'select',
    '#title' => 'Service',
    '#default_value' => variable_get('simplenews_bmh_inbox_remote_service', 'imap'),
    '#options' => array('imap' => 'IMAP', 'pop' => 'POP3'),
  );
  $form['remote']['simplenews_bmh_inbox_remote_secure'] = array(
    '#type' => 'select',
    '#title' => 'Secure connection',
    '#default_value' => variable_get('simplenews_bmh_inbox_remote_secure', 'notls'),
    '#options' => array('notls' => 'None', 'ssl' => 'SSL', 'tls' => 'TLS'),
  );
  
  
  // count rules
  $one2ten  = range(0,10);
  $form['scoring'] = array(
    '#type' => 'fieldset',
    '#title' => 'Scoring',
    '#tree' => FALSE,
  );
  
  /*
   * don't think this is possible based on what phpmailer-bmh returns
   * 
  // per-list counters?
  $form['scoring']['simplenews_bmh_score_global'] = array(
    '#type' => 'checkbox',
    '#title' => 'Global scoring?',
    '#description' => 'Keep one overall bounce score for a given email address? (Disable this to evaluate the bounce score per list.)',
    '#default_value' => variable_get('simplenews_bmh_score_global', 1),
  );
  */
  
  // the score is over what time period?
  $form['scoring']['simplenews_bmh_score_interval'] = array(
    '#type' => 'select',
    '#title' => 'Scoring interval',
    '#description' => 'The score should be calculated for what time interval?',
    '#default_value' => variable_get('simplenews_bmh_score_interval', 0),
    '#options' => array(
      '0'        => 'All time',
      '604800'   => 'The past week',
      '2419200'  => 'The past month',
      '31557600' => 'The past year',
    ),
  );
  
  // threshold to disable account
  // per-list counters?
  $form['scoring']['simplenews_bmh_score_threshold'] = array(
    '#type' => 'select',
    '#title' => 'Bounce threshold',
    '#description' => 'What score (within the set time frame) results in an address being removed?',
    '#default_value' => variable_get('simplenews_bmh_score_threshold', 20),
    '#options' => range(0,100),
  );
  
  
  $form['rules'] = array(
    '#type' => 'fieldset',
    '#title' => 'Rules',
    '#description' => 'There are various kinds of bounces. How much does each kind count towards the total score?',
    '#tree' => FALSE,
  );
  
  // values for various bounce types
  $zero2ten = range(0,10);
  $bounce_types = _simplenews_bmh_bounce_types();
  foreach ($bounce_types as $code => $data) {
    $form['rules']["simplenews_bmh_score_$code"] = array(
      '#type' => 'select',
      '#title' => $code,
      '#description' => $data['desc'],
      '#default_value' => variable_get("simplenews_bmh_score_$code", $data['default']),
      '#options' => $zero2ten,
    );
  }
  
  $form['#submit'][] = 'simplenews_bmh_admin_settings_submit';
  
  return system_settings_form($form);
}

function simplenews_bmh_admin_settings_validate($form, &$form_state) {
  // if there is a local mailbox, we need to know what kind
  /*
  if (!empty($form_state['values']['simplenews_bmh_inbox_local_path']) && empty($form_state['values']['simplenews_bmh_inbox_local_type'])) {
    form_set_error('mailbox][simplenews_bmh_inbox_local_type', 'The type of mailbox is required if you are using a local mailbox.');
  }
  */
  
  if (!is_numeric($form_state['values']['simplenews_bmh_process_maxmessages'])) {
    form_set_error('general][simplenews_bmh_process_maxmessages', 'Invalid entry.');
  }
}


// submit handler for admin settings
// if the scoring has changed, set flag to trigger recalculation
function simplenews_bmh_admin_settings_submit($form, &$form_state) {
  $recalculate = FALSE;
  $zero2ten    = range(0,10);
  $bounce_types = _simplenews_bmh_bounce_types();
  foreach ($bounce_types as $code => $data) {
    $current  = variable_get("simplenews_bmh_score_$code", $data['default']);
    $fromform = $form_state['values']["simplenews_bmh_score_$code"];
    if (in_array($fromform, $zero2ten) !== FALSE && $fromform != $current) {
      $recalculate = TRUE;
      break;
    }
  }
  if ($recalculate) variable_set('simplenews_bmh_recalculate', TRUE);
}


// report on current scores
function simplenews_bmh_bounce_report($email = FALSE) {  
  $path = libraries_get_path('PHPMailer-BMH');
  define('_PATH_BMH', $path . '/');
  require_once(_PATH_BMH . 'phpmailer-bmh_rules.php');   // provides $rule_categories
  
  $limit = variable_get('simplenews_bmh_score_threshold', 20);
  
  // overall report
  if (!$email) {
    $header = array(
      array(
        'data'  => 'email address',
        'field' => 'email',
        'sort'  => 'asc',
      ), 
      array(
        'data'  => 'total score',
        'field' => 'score',
        'sort'  => 'asc',
      ), 
      array(
        'data'  => '',
      ), 
      array(
        'data'  => '',
      ), 
    );
    
    // are we calculating over a period of time?
    $time_constraint = _simplenews_bmh_sql_timeconstraint();
    if ($time_constraint) $header[1]['data'] = 'time period score';

    $dest  = drupal_get_destination();
    $page  = 20;
    $count = "SELECT COUNT(DISTINCT(email)) FROM {simplenews_bmh_bounces}";
    if ($time_constraint) $count .= " WHERE $time_constraint";
    $sql   = "SELECT DISTINCT email, SUM(score) AS score FROM {simplenews_bmh_bounces}";
    if ($time_constraint) $sql .= " WHERE $time_constraint";
    $sql  .= " GROUP BY email" . tablesort_sql($header);
    $result = pager_query($sql, $page, 0, $count);
    while ($row = db_fetch_object($result)) {
      $link  = l($row->email, 'admin/content/simplenews/bounce_report/' . $row->email . "/1", array('query' => $dest));
      $apply = ($row->score > $limit) ? l('Remove', 'simplenews_bmh/bounce_process/' . $row->email . "/1", array('query' => $dest)) : '';
      $clear = l('Clear', 'simplenews_bmh/bounce_clear/' . $row->email . "/1", array('query' => $dest));
      $data[] = array($link, $row->score, $apply, $clear);
    }
    
    return theme('simplenews_bmh_report_summary', $header, $data);
  }
  else {
    $sql = "SELECT * FROM {simplenews_bmh_bounces} WHERE email = '%s';";
    $result = db_query($sql, $email);
    while ($row = db_fetch_object($result)) {
      $category = $row->rule_cat . ' (' . $rule_categories[$row->rule_cat]['bounce_type'] . ')';
      $data[] = array(format_date($row->timestamp, 'medium'), $row->subject, $row->rule_no, $category, $row->score);
    }
    $header = array('date', 'subject',  'rule no.', 'category','score');
    
    return theme('simplenews_bmh_report_single', $header, $data, $email);
  }
}


/*
 * Implementation of hook_theme
 */
function simplenews_bmh_theme() {
  $themes = array();
  
  $themes['simplenews_bmh_report_summary'] = array(
    'arguments' => array(
      'header' => array(), 
      'data'   => array(),
    ),
  );
    
  $themes['simplenews_bmh_report_single'] = array(
    'arguments' => array(
      'header' => array(), 
      'data'   => array(),
      'email'  => FALSE,
    ),
  );
  
  return $themes;
}


// theme function for overall report
function theme_simplenews_bmh_report_summary($header = array(), $data = array()) {
  drupal_set_title('Bounced Mail Report');
  
  $limit = variable_get('simplenews_bmh_score_threshold', 20);
  $dest  = drupal_get_destination();
  $img   = '<img src="/misc/menu-collapsed.png" alt=">" />&nbsp;';
  $page_header  = '<div><p><strong>The bounce score limit is ' . $limit . '</strong>. Select an email address to view its bounce history, use "Remove" to unsubscribe it from all lists, use "Clear" to erase its bounce history, or use the "Remove all bouncing emails" link to apply the bounce score to all subscribers.<p></div>';
  $page_header .= '<div><p>' . $img . l('Remove all bouncing emails', 'simplenews_bmh/bounce_process/0/1', array('query' => $dest)) . '</p></div>';
  
  return $page_header . theme('pager') . theme_table($header, $data) . theme('pager');
}


// theme function for single email report
function theme_simplenews_bmh_report_single($header = array(), $data = array(), $email = FALSE) {
  drupal_set_title("Bounce report for $email");
  $dest = drupal_get_destination();
  
  // some HTML snippets
  $div   = 'style="margin-top: 1em;"';
  $img   = '<img src="/misc/menu-collapsed.png" alt=">" />&nbsp;';
  
  // admin links
  $link1 = $img . l('Apply bounce handling rules to ' . $email, 'simplenews_bmh/bounce_process/' . $email . '/1', array('query' => 'destination=admin/content/simplenews/bounce_report/' . $email));
  $link2 = $img . l('Clear bounce records for ' . $email, 'simplenews_bmh/bounce_clear/' . $email . '/1', array('query' => 'destination=admin/content/simplenews/bounce_report/' . $email));
  $link3 = $img . l('Return to list', 'admin/content/simplenews/bounce_report', array('query' => $dest));

  return theme_table($header, $data) . "\n<div $div>$link1</div>\n" . "\n<div $div>$link2</div>\n" . "\n<div $div>$link3</div>\n";
}

